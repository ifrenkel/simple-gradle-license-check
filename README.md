# License check

Simple gradle license check to verify no unknown licenses.

Can be viewed in `Secure > Dependency List` and `Secure > License Compliance`.